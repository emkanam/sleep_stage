import pandas as pd
from os import listdir
from os.path import isfile, join


class DataLoader(object):
	@staticmethod
	def load_data(fold, norm=False):
		li = []
		path = fold
		files = [f for f in listdir(path) if isfile(join(path, f)) and ".csv" in f]
		
		for file_ in files:
			dat = pd.read_csv(join(path, file_))
			if norm:
				min_ = dat.min()
				max_ = dat.max()
				dat = (dat-(min_+max_)/2)/(max_-min_)
			li.append(dat)
		
		x = pd.concat(li, axis=1)
		return x


if __name__ == "__main__":
	fold = "./train/"
	
	dt = DataLoader.load_data(fold, norm=True)
	print(dt.head(10))