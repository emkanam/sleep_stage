from classification.feature import *
from scipy.fftpack import rfft
import scipy.stats as sst

"""
-- Entropy based features --

SpEn, REnt, ApEn, PEnt, CEnt
"""


class SpEn(Feature):
	"""Computes the Spectral entropy of a vector:
	Return: a DataFrame.
	
	https://fr.mathworks.com/help/signal/ref/pentropy.html
	"""
	
	def __init__(self):
		super(SpEn, self).__init__()
		self.desc_ = " Feature"
		self.name_ = "SpEn"

	def extract(self, x, save='', col_name='', **kwargs):
		
		#def spen(row):
		#	N = len(row)
		#	X = rfft(row) # Fourier transform of x
		#	S = np.abs(X)**2 # Signal power
		#	P = S/np.sum(S) # Density distribution
		#	
		#	H = -np.sum(P*np.log(P))/np.log(N)
		#	return H
		#
		#y = np.apply_along_axis(spen, 1, x)
		#return self.save(y, save=save, col_name=col_name)
		
		return None
		

class REnt(Feature):
	"""Computes the Rényi entropy of a vector:
	Return: a DataFrame.
	
	https://fr.wikipedia.org/wiki/Entropie_de_R%C3%A9nyi
	"""
	
	def __init__(self):
		super(REnt, self).__init__()
		self.desc_ = "Rényi entropy Feature"
		self.name_ = "REnt"

	def extract(self, x, q=2, save='', col_name='', **kwargs):
		
		def rent(row):
			N = len(row)
			X = rfft(row) # Fourier transform of x
			S = np.abs(X)**2 # Signal power
			P = S/np.sum(S) # Density distribution
			
			H = np.log(np.sum(P**q))/(1-q)
			return H
		
		y = np.apply_along_axis(rent, 1, x)
		return self.save(y, save=save, col_name=col_name)
		

class ApEn(Feature):
	"""Computes the Approximate entropy of a vector:
	Return: a DataFrame.
	
	https://en.wikipedia.org/wiki/Approximate_entropy
	"""
	
	def __init__(self):
		super(ApEn, self).__init__()
		self.desc_ = "Approximate entropy Feature"
		self.name_ = "ApEn"

	def extract(self, x, m=2, r=0.15, save='', col_name='', **kwargs):
		# _, N = x.shape
		# 
		# def _phi(m):
		# 	x_ = np.array([x[:, i:i + m - 1 + 1] for i in range(N - m + 1)])
		# 	ln, l, cl = x_.shape
		# 	x_ = x_.reshape(l, ln, cl)
		# 	print(x_.shape)
		# 	
		# 	xr = np.repeat(x_.reshape(-1, 1,ln,cl), ln, axis=1)
		# 	yr = np.repeat(x_.reshape(-1, ln, 1,cl), ln, axis=2)
		# 	dr = np.abs(xr-yr).max(3)
		# 	C = (dr <= r).sum(1) / (N - m + 1.0) 
		# 	print(C)
		# 	return sum(np.log(C)) / (N - m + 1.0) 
		
		# y = abs(_phi(m + 1) - _phi(m))
		# return self.save(y, save=save, col_name=col_name)
		return None


class PEnt(Feature):
	"""Computes the Permutation entropy of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(PEnt, self).__init__()
		self.desc_ = "Permutation entropy Feature"
		self.name_ = "PEnt"

	def extract(self, x, m=2, l=1, save='', col_name='', **kwargs):
		
		# def pent(row):
		# 	x_ = np.array([row[np.arange(j, j+(m-1)*l, l)] for i in range(N - (m - 1)*l)])
		# 
		# return self.save(y, save=save, col_name=col_name)
		return None
	


class CEnt(Feature):
	"""Computes the custom entropy of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(CEnt, self).__init__()
		self.desc_ = "custom entropy Feature"
		self.name_ = "CEnt"

	def extract(self, x, freq=50, save='', col_name='', **kwargs):
		def cent(row):
			n = row.shape[0]
			bins = np.linspace(row.min(),row.max(),200)
			hst, _ = np.histogram(row, bins=bins)
			return sst.entropy(hst/float(n))
		
		y = np.apply_along_axis(cent, 1, x)
		return self.save(y, save=save, col_name=col_name)


def entropy_ftrs():
	ent_ftrs = {
		"SpEn": SpEn(), "REnt": REnt(),
		"ApEn": ApEn(), "PEnt": PEnt(), "CEnt": CEnt()
	}
	
	return ent_ftrs


if __name__ == "__main__":
	import h5py
	import matplotlib.pyplot as plt

	base_path = "../data/" 
	train_hfile = h5py.File(base_path+'X_train.h5', 'r')
	
	x1 = train_hfile['eeg_1'][...]
	x2 = train_hfile['eeg_2'][...]
	
	ft_name = "REnt"
	ft = REnt()
	y1 = ft.extract(x1)
	y2 = ft.extract(x2)
	
	y_train = pd.read_csv(base_path+"y_train.csv")
	cols = y_train["sleep_stage"]
	plt.scatter(y1[ft_name].values, y2[ft_name].values, c=cols)
	plt.show()
	ent_ftrs = entropy_ftrs()
	for e in ent_ftrs:
		print(e, ent_ftrs[e].name_)
		ent_ftrs[e].extract(x1)
