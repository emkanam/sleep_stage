from classification.feature import *
from scipy.fftpack import fft, rfft, fftfreq

"""
-- Frequency and time-frequency based features --

WVdist, DWtrans, FFTrans
"""


class WVdist(Feature):
	"""Computes the Wigner-Ville distribution of a vector:
	Return: a DataFrame.
	
	https://fr.mathworks.com/help/signal/ref/pentropy.html
	"""
	
	def __init__(self):
		super(WVdist, self).__init__()
		self.desc_ = "Wigner-Ville distribution Feature"
		self.name_ = "WVdist"

	def extract(self, x, save='', col_name='', **kwargs):
		# return self.save(y, save=save, col_name=col_name)
		return None


class DWtrans(Feature):
	"""Computes the Discrete wavelet transform of a vector:
	Return: a DataFrame.
	
	https://fr.mathworks.com/help/signal/ref/pentropy.html
	"""
	
	def __init__(self):
		super(DWtrans, self).__init__()
		self.desc_ = "Discrete wavelet transform Feature"
		self.name_ = "DWtrans"

	def extract(self, x, save='', col_name='', **kwargs):
		# return self.save(y, save=save, col_name=col_name)
		return None



class FFTrans(Feature):
	"""Computes the fourier signature transform of a vector:
	Return: a DataFrame.
	"""
	
	def __init__(self):
		super(FFTrans, self).__init__()
		self.desc_ = "Fourier signature Feature"
		self.name_ = "FFTrans"

	def extract(self, x, freq=50, save='', col_name='', **kwargs):
		def fftr(row):
			N = row.shape[0]
			T = 1.0 / freq
			yf = rfft(row)
			dyf = (np.roll(yf, -1) - yf)[:-1]
			xf = fftfreq(N, T)
			dxf = (np.roll(xf, -1) - xf)[:-1]
			return np.mean(dxf*dyf)
		
		y = np.apply_along_axis(fftr, 1, x)
		return self.save(y, save=save, col_name=col_name)


def freq_ftrs():
	fq_ftrs = {
		"WVdist": WVdist(), "DWtrans": DWtrans(),
		"FFTrans": FFTrans()
	}
	
	return fq_ftrs


if __name__ == "__main__":
	import h5py
	import matplotlib.pyplot as plt

	base_path = "../data/" 
	train_hfile = h5py.File(base_path+'X_train.h5', 'r')
	
	x1 = train_hfile['eeg_1'][...]
	# x2 = train_hfile['eeg_2'][...]
	
	# ft_name = "WVdist"
	# ft = WVdist()
	# y1 = ft.extract(x1)
	# y2 = ft.extract(x2)
	
	# y_train = pd.read_csv(base_path+"y_train.csv")
	# cols = y_train["sleep_stage"]
	# plt.scatter(y1[ft_name].values, y2[ft_name].values, c=cols)
	# plt.show()
	ent_ftrs = freq_ftrs()
	for e in ent_ftrs:
		print(e, ent_ftrs[e].name_)
		ent_ftrs[e].extract(x1)
