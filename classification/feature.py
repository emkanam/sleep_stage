# Importing lybrairies
import pandas as pd
import numpy as np


class Feature(object):
	def __init__(self):
		super(Feature, self).__init__()
		self.desc_ = "Feature object"
		self.name_ = "Feature"
		
	def extract(self, x, save='', col_name='', **kwargs):
		raise NotImplementedError("This function needs to be implemented.")
		
	def save(self, y, save='', col_name=''):
		if save != '':
			col_name = self.name_ + "_" + col_name
			y = pd.DataFrame({col_name: y})
			y.to_csv(save+col_name+".csv", index=False)
		else:
			col_name = self.name_
			y = pd.DataFrame({col_name: y})
		return y

	def __str__(self):
		return self.desc_
