import numpy as np
from scipy.signal import butter, lfilter


def mav_smooth(x, k=10):
	
	def rsmooth(row):
		y = row.copy()
		for i in range(1, k):
			y += np.hstack([row[i:-k+i], row[-k-i:-(i)]])
		return y/k
	
	y = np.apply_along_axis(rsmooth, 1, x)
	return y


class BandFilter(object):
	def __init__(self, lowc, highc):
		super(BandFilter, self).__init__()
		self.lowc = lowc
		self.highc = highc
	
	def run(self, x, fs, order=4, save='', **kwargs):
		def rfilter(row):
			nyq = 0.5 * fs
			low = self.lowc / nyq
			high = self.highc / nyq
			b, a = butter(order, [low, high], btype='band')

			y = lfilter(b, a, row)
			return y
		
		y = np.apply_along_axis(rfilter, 1, x)
		return y


if __name__ == "__main__":
	import h5py
	
	base_path = "../data/" 
	train_hfile = h5py.File(base_path+'X_train.h5', 'r')
	
	x1 = train_hfile['eeg_1'][...]
	
	print(mav_smooth(x1))
	