from classification.feature import *

"""
-- Nonlinear based features --

PFDim, MTEner, MEner, MCLen, HExp, MMDist, Esis
"""


class PFDim(Feature):
	"""Computes the Petrosian fractal dimension of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(PFDim, self).__init__()
		self.desc_ = "Petrosian fractal dimension Feature"
		self.name_ = "PFDim"

	def extract(self, x, freq=50.0, fen=30.0, save='', col_name='', **kwargs):
		k = x.shape[1] # Number of samples
		t = np.linspace(0,fen,int(fen*freq)) # Sample k times in the window
		ds = np.gradient(x, t, axis=1) # compute the gradiant
		sc = ((ds[:,:-1] * ds[:,1:]) < 0).sum(axis=1) # Sign change in the gradiant
		y = np.log10(k)/(np.log10(k) + np.log10( k/(k + 0.4*sc) ))
		
		return self.save(y, save=save, col_name=col_name)


class MTEner(Feature):
	"""Computes the Mean teager energy of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(MTEner, self).__init__()
		self.desc_ = "Mean teager energy Feature"
		self.name_ = "MTEner"

	def extract(self, x, save='', col_name='', **kwargs):
		a = x[:,2:] # x[m]
		a1 = x[:,1:-1] # x[m-1]
		a2 = x[:,:-2] # x[m-2]
		y = (a1**2 - a*a2).mean(axis=1)

		return self.save(y, save=save, col_name=col_name)


class MEner(Feature):
	"""Computes the Mean energy of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(MEner, self).__init__()
		self.desc_ = "Mean energy Feature"
		self.name_ = "MEner"

	def extract(self, x, save='', col_name='', **kwargs):
		y = (x**2).mean(axis=1)
		return self.save(y, save=save, col_name=col_name)
	

class MCLen(Feature):
	"""Computes the Mean curve length of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(MCLen, self).__init__()
		self.desc_ = "Mean curve length Feature"
		self.name_ = "MCLen"

	def extract(self, x, save='', col_name='', **kwargs):
		a = x[:,1:] # x[m]
		a1 = x[:,:-1] # x[m-1]
		y = (np.abs(a-a1)).mean(axis=1)
		
		return self.save(y, save=save, col_name=col_name)
	

class HExp(Feature):
	"""Computes the Hurst exponent of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(HExp, self).__init__()
		self.desc_ = "Hurst exponent Feature"
		self.name_ = "Hexp"

	def extract(self, x, fen=30.0, save='', col_name='', **kwargs):
		m = x.mean(axis=1) # Mean
		z = x-m.reshape(-1,1) # x - mean
		z = np.cumsum(z, axis=1)
		r = z.max(axis=1) - z.min(axis=1)
		s = np.std(x, axis=1) # std
		y = np.log(r/s)/np.log(fen)
		
		return self.save(y, save=save, col_name=col_name)
	

class MMDist(Feature):
	"""Computes the MM-distance of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(MMDist, self).__init__()
		self.desc_ = "MM-distance Feature"
		self.name_ = "Hexp"

	def extract(self, x, freq=50.0, fen=30.0, save='', col_name='', **kwargs):
		def rmmd(row):
			lmd = int(freq)
			N = row.shape[0]
			t = np.linspace(0, fen, N)
			yf = row.reshape((-1, lmd))
			xf = t.reshape((-1, lmd))
			n = yf.shape[0]
			yf_min = yf.argmin(axis=1)
			yf_max = yf.argmax(axis=1)

			dx = xf[np.arange(n), yf_min] - xf[np.arange(n), yf_max]
			dy = yf[np.arange(n), yf_min] - yf[np.arange(n), yf_max]
			
			return np.sum(np.sqrt(dx**2 + dy**2))
		
		y = np.apply_along_axis(rmmd, 1, x)
		
		return self.save(y, save=save, col_name=col_name)
	

class Esis(Feature):
	"""Computes the Esis of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(Esis, self).__init__()
		self.desc_ = "Esis Feature"
		self.name_ = "Esis"

	def extract(self, x, freq=2.0, save='', col_name='', **kwargs):
		def resis(row):
			N = row.shape[0]
			lmb = 100
			if N >= 10000:
				lmb = 10**(np.log(N)-1)
			
			return np.sum((freq * lmb) * row**2)
		
		y = np.apply_along_axis(resis, 1, x)
		
		return self.save(y, save=save, col_name=col_name)


def nonlin_ftrs():
	nl_ftrs = {
		"PFDim": PFDim(), "MTEner": MTEner(), "MEner": MEner(), 
		"MCLen": MCLen(), "HExp": HExp(), "MMDist": MMDist(),
		"Esis": Esis()
	}
	
	return nl_ftrs
	


if __name__ == "__main__":
	import h5py
	import matplotlib.pyplot as plt

	base_path = "../data/" 
	train_hfile = h5py.File(base_path+'X_train.h5', 'r')
	
	x1 = train_hfile['eeg_1'][...]
	# x2 = train_hfile['eeg_2'][...]
	
	ft_name = "HExp"
	# ft = HExp()
	# y1 = ft.extract(x1)
	# y2 = ft.extract(x2)
	
	# y_train = pd.read_csv(base_path+"y_train.csv")
	# cols = y_train["sleep_stage"].values
	# plt.scatter(y1[ft_name].values, y2[ft_name].values, c=cols)
	# plt.show()
	nl_ftrs = nonlin_ftrs()
	for e in nl_ftrs:
		print(e, nl_ftrs[e].name_)
		nl_ftrs[e].extract(x1)
