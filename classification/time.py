from classification.feature import *
import scipy.stats as sst

"""
-- Time based features --

Statistical features: AMean, MaxV, MinV, Stdev, 
Var,MedN, Skew, Kurt

Other features: ZCros, HAct, HMob, HComp
"""


class AMean(Feature):
	"""Computes the arithmetic mean of a vector:
	\frac{1}{N}\sum_{n=1}^N(x_n)
	Return: a DataFrame."""
	
	def __init__(self):
		super(AMean, self).__init__()
		self.desc_ = "Arithmetic Mean Feature"
		self.name_ = "AMean"

	def extract(self, x, save='', col_name='', **kwargs):
		y = np.mean(x, axis=1)
		return self.save(y, save=save, col_name=col_name)
	

class MaxV(Feature):
	"""Computes the maximum value of a vector:
	\max_{n=1}^N(x_n)
	Return: a DataFrame."""
	
	def __init__(self):
		super(MaxV, self).__init__()
		self.desc_ = "Maximum Value Feature"
		self.name_ = "MaxV"

	def extract(self, x, save='', col_name='', **kwargs):
		y = np.max(x, axis=1)
		return self.save(y, save=save, col_name=col_name)
	

class MinV(Feature):
	"""Computes the minimum value of a vector:
	\min_{n=1}^N(x_n)
	Return: a DataFrame."""
	
	def __init__(self):
		super(MinV, self).__init__()
		self.desc_ = "Minimum Value Feature"
		self.name_ = "MinV"

	def extract(self, x, save='', col_name='', **kwargs):
		y = np.min(x, axis=1)
		return self.save(y, save=save, col_name=col_name)
	

class Stdev(Feature):
	"""Computes the standard devaition of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(Stdev, self).__init__()
		self.desc_ = "Standard Deviation Feature"
		self.name_ = "Stdev"

	def extract(self, x, save='', col_name='', **kwargs):
		y = np.std(x, axis=1)
		return self.save(y, save=save, col_name=col_name)


class Var(Feature):
	"""Computes the Variance of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(Var, self).__init__()
		self.desc_ = "Variance Feature"
		self.name_ = "Var"

	def extract(self, x, save='', col_name='', **kwargs):
		y = np.var(x, axis=1)
		return self.save(y, save=save, col_name=col_name)


class MedN(Feature):
	"""Computes the Median of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(MedN, self).__init__()
		self.desc_ = "Median Value Feature"
		self.name_ = "MedN"

	def extract(self, x, save='', col_name='', **kwargs):
		y = np.median(x, axis=1)
		return self.save(y, save=save, col_name=col_name)


class Skew(Feature):
	"""Computes the Skewness of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(Skew, self).__init__()
		self.desc_ = "Skewness Feature"
		self.name_ = "Skew"

	def extract(self, x, save='', col_name='', **kwargs):
		y = sst.skew(x, axis=1)
		return self.save(y, save=save, col_name=col_name)


class Kurt(Feature):
	"""Computes the Kurthosis of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(Kurt, self).__init__()
		self.desc_ = "Kurthosis Feature"
		self.name_ = "Kurt"

	def extract(self, x, save='', col_name='', **kwargs):
		y = sst.kurtosis(x, axis=1)
		return self.save(y, save=save, col_name=col_name)


class ZCros(Feature):
	"""Computes the number of zero crossing of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(ZCros, self).__init__()
		self.desc_ = "Zero Crossing Feature"
		self.name_ = "ZCros"

	def extract(self, x, save='', col_name='', **kwargs):
		y = ((x[:,:-1] * x[:,1:]) < 0)
		y += (x[:,:-1] != 0) * (x[:,1:] == 0)
		y = y.sum(axis=1)
		return self.save(y, save=save, col_name=col_name)


class HAct(Feature):
	"""Computes the Hjorth activity (varience) of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(HAct, self).__init__()
		self.desc_ = "Hjorth Activity Feature"
		self.name_ = "HAct"

	def extract(self, x, save='', col_name='', **kwargs):
		y = np.var(x, axis=1)
		return self.save(y, save=save, col_name=col_name)
	

class HMob(Feature):
	"""Computes the Hjorth mobility (varience) of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(HMob, self).__init__()
		self.desc_ = "Hjorth Mobility Feature"
		self.name_ = "HMob"

	def extract(self, x, freq=50.0, fen=30.0, save='', col_name='', **kwargs):
		t = np.linspace(0, fen, int(fen*freq))
		sigma_0 = np.std(x, axis=1)
		sigma_1 = np.gradient(x, t, axis=1)
		sigma_1 = np.std(sigma_1, axis=1)
		y = sigma_1/sigma_0
		
		return self.save(y, save=save, col_name=col_name)
	

class HComp(Feature):
	"""Computes the Hjorth complexity (varience) of a vector:
	Return: a DataFrame."""
	
	def __init__(self):
		super(HComp, self).__init__()
		self.desc_ = "Hjorth Complexity Feature"
		self.name_ = "HComp"

	def extract(self, x, freq=50.0, fen=30.0, save='', col_name='', **kwargs):
		t = np.linspace(0, fen, int(fen*freq))
		sigma_0 = np.std(x, axis=1)
		sigma_1 = np.gradient(x, t, axis=1)
		sigma_2 = np.gradient(sigma_1, t, axis=1)
		sigma_1 = np.std(sigma_1, axis=1)
		sigma_2 = np.std(sigma_2, axis=1)
		y = (sigma_2/sigma_1) - (sigma_1/sigma_0)**2
		
		return self.save(y, save=save, col_name=col_name) 


def time_ftrs():
	t_ftrs = {
		"AMean": AMean(), "MaxV": MaxV(), "MinV": MinV(), 
		"Stdev": Stdev(), "Var": Var(), "MedN": MedN(), 
		"Skew": Skew(), "Kurt": Kurt(),
		"ZCros": ZCros(), "HAct": HAct(), "HMob": HMob(), 
		"HComp": HComp()
	}
	
	return t_ftrs
	


if __name__ == "__main__":
	import h5py
	import matplotlib.pyplot as plt

	base_path = "../data/" 
	train_hfile = h5py.File(base_path+'X_train.h5', 'r')
	
	x1 = train_hfile['eeg_1'][...]
	# x2 = train_hfile['eeg_2'][...]
	
	ft_name = "HComp"
	# ft = HComp()
	# y1 = ft.extract(x1)
	# y2 = ft.extract(x2)
	
	# y_train = pd.read_csv(base_path+"y_train.csv")
	# cols = y_train["sleep_stage"].values
	# plt.scatter(y1[ft_name].values, y2[ft_name].values, c=cols)
	# plt.show()
	
	t_ftrs = time_ftrs()
	for e in t_ftrs:
		print(e, t_ftrs[e].name_)
		t_ftrs[e].extract(x1)
