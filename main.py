from classification.entropy import entropy_ftrs
from classification.frequency import freq_ftrs
from classification.nonlinear import nonlin_ftrs
from classification.time import time_ftrs

from classification.filters import mav_smooth, BandFilter


if __name__ == "__main__":
	import h5py
	base_path = "./data/"
	file = "test"
	
	ftr_50hz = ['eeg_1', 'eeg_2', 'eeg_3', 'eeg_4', 'eeg_5', 'eeg_6', 'eeg_7']
	ftr_10hz = ['pulse', 'x', 'y', 'z']
	
	dtfile = h5py.File(base_path+"X_" + file + ".h5", 'r')
	
	# Get feature extractors
	ent_ftrs = entropy_ftrs()
	fq_ftrs = freq_ftrs()
	nl_ftrs = nonlin_ftrs()
	t_ftrs = time_ftrs()
	
	# filter
	flt = BandFilter(7.5, 15)
	flt_name = "REM"
	
	# Extract EEG features
	save_folder = base_path + file + "/"
	for d_col in ftr_50hz:
		x = dtfile[d_col][...]
		x = flt.run(x, 50)
		d_col = d_col + flt_name
		
		for e in ent_ftrs:
			ent_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=50.0, fen=30.0)
		for e in fq_ftrs:
			fq_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=50.0, fen=30.0)
		for e in nl_ftrs:
			nl_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=50.0, fen=30.0)
		for e in t_ftrs:
			t_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=50.0, fen=30.0)
	
	# for d_col in ftr_10hz:
	# 	x = dtfile[d_col][...]
	# 	for e in ent_ftrs:
	# 		ent_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
	# 	for e in fq_ftrs:
	# 		fq_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
	# 	for e in nl_ftrs:
	# 		nl_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
	# 	for e in t_ftrs:
	# 		t_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
		
	# d_col = "xyz"
	# x = dtfile["x"][...]
	# y = dtfile["y"][...]
	# z = dtfile["z"][...]
	# 
	# x = (x**2 + y**2 + z**2)**0.5
	# x = mav_smooth(x)
	# for e in ent_ftrs:
	# 	ent_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
	# for e in fq_ftrs:
	# 	fq_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
	# for e in nl_ftrs:
	# 	nl_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
	# for e in t_ftrs:
	# 	if e != "ZCros":
	# 		t_ftrs[e].extract(x, save=save_folder, col_name=d_col, q=2, m=2, r=0.15, l=1, freq=10.0, fen=30.0)
	